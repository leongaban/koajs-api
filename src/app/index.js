const Koa = require('koa')
const router = require('routing')
const Morgan = require('koa-morgan')
const server = new Koa()

exports.start = async () => {
  server
    .use(require('koa-body')())
    .use(Morgan('dev'))
    .use(router.allowedMethods())
    .use(router.routes())

  try {
    await server.listen(3000)
  } catch(error) {
    console.log(error);
  }

  console.log('Koa server running on port 3000...');
}

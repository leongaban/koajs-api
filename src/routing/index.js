const Router = require('koa-router')
const router = new Router()

// TODO replace with db data
const users = require('models/users')

// ? Home Route
router.get('home', '/', ctx => ctx.body = 'Hello, World!')

// ? GET users
router.get('users', '/users', ctx => ctx.body = users)

// ? GET user
router.get('user', '/user/:id', ctx => ctx.body = users[ctx.params.id])

// ? POST user
router.post('user', '/user/:id', ctx => {
  ctx.body = Object.assign(users[ctx.params.id], ctx.request.body)
})

module.exports = router
